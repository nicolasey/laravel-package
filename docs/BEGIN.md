# Démarrer un package

#### Créer le paquet en lui-même

- Aller sur le dossier racine (ci-après nommé `www`) de tous vos projets, là où vous avez mis celui de la plateforme
- On va supposer que dans `www` vous avez votre projet sur lequel vous voulez passer le module, ci-nommé `project`
- Toujours dans votre dossier `www` créer un projet à partir de ce repository 
- renommer le dossier de ce package dans celui que vous voulez (ex: `cafe-generator` si vous faites un module qui fait le café)
- Adapter le fichier `composer.json`, notamment les variables :

  - Name
  - Description
  - Autoload (les deux)
  - L'autogénération du provider laravel (et les Facades si besoin)

#### Raccorder le nouveau paquet à votre projet Laravel 

En remplaçant par les bonnes valeurs :

- Aller dans le dossier de la plateforme vierge Laravel `project`
- Créer une branche dédiée (ex: `package/cafe-generator` ) 
- `composer config repositories.local '{"type": "path", "url": "../cafe-generator"}' --file composer.json`
- `composer require nicolasey/laravel-package:@dev` 

La variable url de la première requète est le chemin relatif pour aller de `project` à `cafe-generator`

Vous pouvez désormais retourner développer sur le dossier de ce paquet. Vous pourrez ensuite tester les effets en live sur votre plateforme `packages`

