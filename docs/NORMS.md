# Développer un paquet

#### Cloisonnement 

Tous les comportements liés au package doivent être développés dans ce package (c'est même tout l'intérêt). 

Si certains objets utilisent mon générateur de café, alors il faut utiliser les **Traits**
L'avantage des traits est de : 
 - mutualiser en un seul fichier des comportements communs
 - bien séparer la logique de mes modules

#### Gestion des dépendances 

#### Indépendance des tests

